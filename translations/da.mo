��          �   %   �      P  -   Q  (     %   �  $   �  8   �  7   ,  6   d  5   �  %   �  4   �  #   ,  Q   P  Q   �  *   �  *     (   J  '   s  0   �  %   �  ,   �  %     '   E  &   m  #   �  8   �  +  �          ;     O     d  (   v  (   �  !   �  !   �     	  $   	     D	  G   U	  H   �	     �	     �	     
     +
  #   A
     e
     y
     �
     �
     �
     �
  8   �
                                        
                                                                        	             QuickPay API ServiceCapture amount too high. QuickPay API ServiceInvalid parameters. QuickPay API ServiceNo payment link. QuickPay API ServiceNot authorized. QuickPay API ServicePartial payment capture successful. QuickPay API ServicePartial payment refund successful. QuickPay API ServicePayment authorization successful. QuickPay API ServicePayment cancellation successful. QuickPay API ServicePayment created. QuickPay API ServicePayment found, but has no link. QuickPay API ServicePayment found. QuickPay API ServicePayment fully captured, no further payment capture possible. QuickPay API ServicePayment fully refunded, no further payment refunds possible. QuickPay API ServicePayment link created. QuickPay API ServicePayment link deleted. QuickPay API ServicePayment link found. QuickPay API ServicePayment not found. QuickPay API ServicePayment request successful. QuickPay API ServicePayment updated. QuickPay API ServiceRefund amount too high. QuickPay API ServiceScope retrieved. QuickPay API ServiceUnable to capture. QuickPay API ServiceUnable to refund. QuickPay API ServiceUnknown error. Service and API integration with QuickPay's payment API. Project-Id-Version: QuickPay
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Dion Jensen <dion@graloth.com>
Language-Team: Novicell
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
X-Generator: Poedit 2.0.7
X-Poedit-SourceCharset: UTF-8
 Indfangningsbeløb for højt. Ugyldige parametre. Intet betalingslink. Ikke autoriseret. Delvis betalingsindfangning successfuld. Delvis betalingsrefundering successfuld. Betalingsgodkendelse successfuld. Betalingsannullering successfuld. Betaling oprettet. Betaling fundet, men har intet link. Betaling fundet. Betaling fuldt indfanget, ingen yderligere betalingsindfangning muligt. Betaling fuldt refunderet, ingen yderligere betalingsrefundering muligt. Betalingslink oprettet. Betalingslink slettet. Betalingslink fundet. Betaling ikke fundet. Betalingsforespørgsel successfuld. Betaling opdateret. Refunderingsbeløb for højt. Omfang hentet. Indfanging ikke muligt. Refundering ikke muligt. Ukendt fejl. Service og API integration med QuickPay's betalings API. 