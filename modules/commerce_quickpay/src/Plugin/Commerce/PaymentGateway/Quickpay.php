<?php

namespace Drupal\commerce_quickpay\Plugin\Commerce\PaymentGateway;


use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\quickpay\Service\QuickPayService;
use Money\Currency;
use Money\Money;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the QuickPay offsite redirect Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "quickpay_offsite_redirect",
 *   label = @Translation("QuickPay (Redirect to QuickPay)"),
 *   display_label = @Translation("QuickPay"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_quickpay\PluginForm\OffsiteRedirect\QuickpayOffsiteForm",
 *   },
 * )
 *
 * @package Drupal\commerce_quickpay\Plugin\Commerce\PaymentGateway
 */
class Quickpay extends OffsitePaymentGatewayBase implements HasPaymentInstructionsInterface, SupportsAuthorizationsInterface, SupportsNotificationsInterface, SupportsRefundsInterface {

  /**
   * The QuickPay API service.
   *
   * @var \Drupal\quickpay\Service\QuickPayService
   */
  protected $service;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * @inheritDoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    // Rounder service
    $this->rounder = \Drupal::service('commerce_price.rounder');

    // QuickPay service
    $this->service = \Drupal::service('quickpay.service');

    // Initialize keys
    if (!empty($configuration['api_key']) && !empty($configuration['private_key'])) {
      $this->service->setCredentials($configuration['api_key'], $configuration['private_key']);
    }
  }

  /**
   * @inheritDoc
   */
  public function defaultConfiguration() {
    $default_configuration = [
      'payment_methods' => 'creditcard',
      'private_key' => '',
      'api_key' => '',
      'order_prefix' => '',
      'autofee' => FALSE,
      'autocapture' => FALSE,
      'instructions' => t('Thank you for your payment with QuickPay. We will inform you when your payment is processed. This is usually done within 24 hours.',
        [],
        ['context' => 'QuickPay payment instructions']
      )->render(),
      'country_fallback' => '',
    ];
    return $default_configuration + parent::defaultConfiguration();
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('This is the private key from the Quickpay manager.'),
      '#default_value' => $this->configuration['private_key'],
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('The API key for the same user as used in Agreement ID.'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    $form['order_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order ID prefix'),
      '#description' => $this->t('Prefix for order IDs. Order IDs must be uniqe when sent to QuickPay, use this to resolve clashes.'),
      '#default_value' => $this->configuration['order_prefix'],
    ];

    $form['payment_methods'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Accepted payment methods'),
      '#description' => $this->t('Which payment methods to accept.'),
      '#default_value' => $this->configuration['payment_methods'],
    ];

    $form['autofee'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autofee'),
      '#description' => $this->t('If set, the fee charged by the acquirer will be calculated and added to the transaction amount.'),
      '#default_value' => $this->configuration['autofee'],
    ];

    $form['autocapture'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autocapture'),
      '#description' => $this->t('If set, the transactions will be automatically captured.'),
      '#default_value' => $this->configuration['autocapture'],
    ];

    $form['instructions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Payment instructions'),
      '#default_value' => $this->configuration['instructions'],
      '#rows' => 4,
    ];

    $request = \Drupal::request();

    $host = $request->getHttpHost();

    $form['country_fallback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country fallback'),
      '#description' => $this->t('In case the <a href="@iso" target="_blank">ISO 2166-1-alpha-3</a> Country Code could not be parsed, please enter a fallback here.',
        ['@iso' => 'https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3']),
      '#default_value' => $this->configuration['country_fallback'],
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['private_key'] = $values['private_key'];
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['order_prefix'] = $values['order_prefix'];
      $this->configuration['payment_methods'] = $values['payment_methods'];
      $this->configuration['autofee'] = $values['autofee'];
      $this->configuration['autocapture'] = $values['autocapture'];
      $this->configuration['instructions'] = $values['instructions'];
      $this->configuration['country_fallback'] = $values['country_fallback'];
      $this->service->setCredentials($values['api_key'], $values['private_key']);
    }
  }

  /**
   * Create a payment with QuickPay.
   *
   * @param PaymentInterface $payment
   *   The commerce payment.
   * @param array $parameters
   *   An array of parameters used to create the remote payment.
   * @return bool|string
   *   Returns the URL for the payment page if successful, otherwise returns FALSE.
   */
  public function createRemotePayment(PaymentInterface $payment, array $parameters) {
    $payment_response = $this->generatePayment($payment);

    if ($payment_response->success) {
      // Set the payments remove ID
      if ($payment_response->success) {
        try {
          $payment->setRemoteId($payment_response->payment->id);
          $payment->save();
        } catch (EntityStorageException $e) {
          \Drupal::logger('Commerce QuickPay')->critical('<strong>'.$e->getMessage().'</strong> : <br /><pre>'.$e->getTraceAsString().'</pre>');
        }
      }
      $link_response = $this->generateLink($payment, $parameters);
      if ($link_response->success) {
        \Drupal::logger('Commerce QuickPay')->info('Payment link creation success');
        return $link_response->url;
      } else {
        \Drupal::logger('Commerce QuickPay')->error('Payment link creation failed');
        return $link_response;
      }
    } else {
      \Drupal::logger('Commerce QuickPay')->error('Payment creation failed');
      return $payment_response;
    }
  }

  /**
   * @inheritDoc
   */
  public function getNotifyUrl() {
    $url = Url::fromRoute('commerce_payment.notify')
      ->setRouteParameter('commerce_payment_gateway', $this->entityId)
      ->setAbsolute(TRUE)
      ->setOption('base_url', $this->configuration['callback_domain'])
      ->setOption('https', FALSE);
    return $url;
  }

  /**
   * @inheritDoc
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);
    // Do nothing, as QuickPay does not return any data with their redirect,
    // so we have nothing to properly work on, as we need to verify the data with QuickPay.
  }

  /**
   * @inheritDoc
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
    // Payment was cancelled by the user, this does not mean it is cancelled in QuickPay, there it
    // is still sitting in the "initial" state, allowing us to update it in the future.

    try {
      /** @var PaymentInterface $payment */
      $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load($request->query->get('payment'));
      // We set the payment state to "cancelled" so it's visible in the backend.
      $payment->setState('Cancelled');
      // We set the expised time to now, to make it expire immidiately.
      $payment->setExpiresTime(\Drupal::time()->getRequestTime());
      $payment->save();
    } catch (EntityStorageException $e) {
      \Drupal::logger('Commerce QuickPay')->critical('<strong>'.$e->getMessage().'</strong> : <br /><pre>'.$e->getTraceAsString().'</pre>');
    } catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('Commerce QuickPay')->critical('<strong>'.$e->getMessage().'</strong> : <br /><pre>'.$e->getTraceAsString().'</pre>');
    }
  }

  /**
   * @inheritDoc
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);
    // This is the callback from QuickPay, we need to validate the response and update our payment/order accordingly.

    // Make sure we only handle requests with JSON data.
    if ( 0 !== strpos( $request->headers->get( 'Content-Type' ), 'application/json' ) ) {
      return Response::create(json_encode([
        'Code' => 400,
        'Message' => 'Bad request'
      ]), 400, []);
    }

    // Only accept payment requests
    if ($request->headers->get('Quickpay-Resource-Type') != 'Payment') {
      return Response::create(json_encode([
        'Code' => 400,
        'Message' => 'Bad request'
      ]), 400, []);
    }

    // Decode the data and replace the request data with it, making it easier to handle.
    $data = json_decode($request->getContent(), TRUE);
    $request->request->replace(is_array($data)? $data : []);

    $quickpay_checksum  = $request->headers->get('Quickpay-Checksum-Sha256');
    $request_body       = $request->getContent(FALSE);
    $private_key        = $this->configuration['private_key'];
    $checksum           = $this->sign($request_body, $private_key);
    $quickpay_id        = $data['id'];
    $order_id           = $data['variables']['commerce_order_id'];

    // Checksums do not match, deny access.
    if ($checksum !== $quickpay_checksum) {
      return Response::create(json_encode([
        'Code' => 401,
        'Message' => 'Access denied!'
      ]), 401, []);
    }
    // Payment is not accepted, prefent future callbacks for this request.
    if (!isset($data['accepted']) || $data['accepted'] != '1') {
      return Response::create(json_encode([
        'Code' => 200,
        'Message' => 'Payment is not accepted. Ignoring.'
      ]), 200, []);
    }

    $payment = $this->getNewestPayment($order_id, $quickpay_id);

    if (!empty($payment)) {
      try {
        $payment->setRemoteState($data['state']);
        $payment->setState('Accepted');
        $payment->setCompletedTime(strtotime($data['updated_at']));
        $payment->setAuthorizedTime(strtotime($data['updated_at']));
        $payment->save();
      } catch (EntityStorageException $e) {
        // We return a HTTP status code of 500, as this exception means that the payment might not have been saved,
        // and that the callback should be attempted again by QuickPay.
        return Response::create(json_encode([
          'Code' => 500,
          'Message' => 'Internal server error.'
        ]), 500, []);
      }
    } else {
      // We could not find any payments matching the supplied data, so we ignore it to prevent future requests
      // of this payment from hammering our server.
      return Response::create(json_encode([
        'Code' => 200,
        'Message' => 'Payment not found.'
      ]), 200, []);
    }

    // Payment was successfully processed
    return Response::create(json_encode([
      'Code' => 200,
      'Message' => 'Payment callback received and processed.'
    ]), 200, []);
  }

  /**
   * @inheritDoc
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [
      '#type' => 'processed_text',
      '#text' => $this->configuration['instructions'],
      '#format' => 'plain_text',
    ];

    return $instructions;
  }

  /**
   * @inheritDoc
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    // We capture the specified amount, if no amount is specified we capture full amount.
    if (!empty($amount)) {
      $money = new Money($amount->getNumber() , new Currency($amount->getCurrencyCode()));
      $this->service->capturePayment($payment->getRemoteId(), $money);
    } else {
      $this->service->capturePayment($payment->getRemoteId());
    }
  }

  /**
   * @inheritDoc
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    // We refund the specified amount, if no amount is specified we refund full amount.
    if (!empty($amount)) {
      $money = new Money($amount->getNumber() , new Currency($amount->getCurrencyCode()));
      $this->service->refundPayment($payment->getRemoteId(), $money);
    } else {
      $this->service->refundPayment($payment->getRemoteId());
    }

  }

  /**
   * @inheritDoc
   */
  public function voidPayment(PaymentInterface $payment) {
    // Commerce Payments do not have a "cancel" function, but use "void" in it's place, so we tell QuickPay to
    // cancel the payment.
    $this->service->cancelPayment($payment->getRemoteId());
  }

  private function generatePayment(PaymentInterface $payment) {
    $order = $payment->getOrder();
    $order_id = $this->configuration['order_prefix'] . sprintf('%04d', $order->id());

    // Does the order have any existing payments?
    $quickpay_payments = $this->service->getPayments($order_id, TRUE);
    $payment_count = count($quickpay_payments->payments);
    /** @var \stdClass|null $quickpay_payment */
    $quickpay_payment = NULL;

    if (!empty($quickpay_payments->payments)) {
      $unpaid = [];
      // Check if any of the payments are unpaid.
      foreach ($quickpay_payments->payments as $response_payment) {
        if ($response_payment->accepted === FALSE && $response_payment->state === 'initial') {
          // We now know the payment was not completed and is in the "initial" state, but we still need to be sure
          // it wasn't cancelled. We do this by making sure it doesn't have a "cancel" operation on it.
          $is_unpaid = TRUE;
          foreach ($response_payment->operations as $operation) {
            if ($operation->type === 'cancel') {
              $is_unpaid = FALSE;
            }
          }

          if ($is_unpaid) {
            // We now know that the payment we have is not cancelled, and unpaid
            $unpaid[] = $response_payment;
          }
        }
      }

      if (!empty($unpaid)) {
        if (count($unpaid) > 1) {
          \Drupal::logger('Commerce QuickPay')->warning(t('Order @orderno contains more than 1 unpaid payment', ['@orderno' => $payment->getOrder()->id()]));
        }

        // Take the first unpaid payment
        $quickpay_payment = $unpaid[0];
      }
    }

    if (!empty($payment_count) && $payment_count > 0) {
      $count_suffix = '_'.($payment_count + 1);
    } else {
      $count_suffix = '';
    }

    $parameters = [
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'order_id' => $order_id.$count_suffix,
      'invoice_address' => $this->getInvoiceAddress($order),
      'basket' => $this->getBasket($order),
      'variables' => [
        'commerce_order_id' => $order->id(), // The clean order id, as 'order_id' might have a prefix and suffix.
      ],
    ];

    // If a, unpaid payment was found, update it.
    if (!empty($quickpay_payment)) {
      // Updating a payment does not support changing the currency or order_id
      unset($parameters['currency']);
      unset($parameters['order_id']);

      // Update it.
      $payment_response = $this->service->updatePayment($quickpay_payment->id, $parameters);
    } else {
      // Otherwise, create a new one.
      $payment_response = $this->service->createPayment($parameters);
    }

    return $payment_response;
  }

  /**
   * Gets the invoice address for the commerce order.
   *
   * @param OrderInterface $order
   *   The commerce order.
   * @return array
   *   Returns a full invoice address array.
   */
  private function getInvoiceAddress(OrderInterface $order) {

    $profile = $order->getBillingProfile();

    // Get the first address only
    $address = NULL;
    foreach ($profile->get('address')->getValue() as $value) {
      $address = $value;
      break;
    }

    $name = [];
    if (empty($name)) {
      if (!empty($address['given_name'])) {
        $name[] = $address['given_name'];
      }
      if (!empty($values['additional_name'])) {
        $name[] = $values['additional_name'];
      }
      if (!empty($values['family_name'])) {
        $name[] = $values['family_name'];
      }
    }

    $address_lines = [];
    if (empty($address_lines)) {
      if (!empty($address['address_line1'])) {
        $address_lines[] = $address['address_line1'];
      }
      if (!empty($values['address_line2'])) {
        $address_lines[] = $values['address_line2'];
      }
    }

    // Properly parse the ISO 3166-1-alpha-2 (2 letters) to ISO 3116-1-alpha-3 (3 letters)
    $codes = json_decode(file_get_contents('http://country.io/iso3.json'), TRUE);

    $alpha2_code = strtoupper($address['country_code']);
    if (isset($codes[$alpha2_code])) {
      $alpha3_code = $codes[$alpha2_code];
    } else {
      $alpha3_code = $this->configuration['country_fallback'];
    }

    if (empty($address['organization'])) {
      $result['name'] = implode(' ', $name);
    }
    if (!empty($address['organization'])) {
      $result['att'] = implode(' ', $name);
    }
    if (!empty($address['organization'])) {
      $result['company_name'] = $address['organization'];
    }
    if (!empty($address_lines)) {
      $result['street'] = implode(', ', $address_lines);
    }
    if (!empty($address['locality'])) {
      $result['city'] = $address['locality'];
    }
    if (!empty($address['postal_code'])) {
      $result['zip_code'] = $address['postal_code'];
    }
    if (!empty($alpha3_code)) {
      $result['country_code'] = $alpha3_code;
    }
    if ($profile->hasField('field_cvr') && !$profile->get('field_cvr')->isEmpty()) {
      $result['vat_no'] = $profile->get('field_cvr')->first()->value;
    }
    if ($profile->hasField('field_phone') && !$profile->get('field_phone')->isEmpty()) {
      $result['phone_number'] = $profile->get('field_phone')->first()->value;
    }
    if ($profile->hasField('field_mobile') && !$profile->get('field_mobile')->isEmpty()) {
      $result['mobile_number'] = $profile->get('field_mobile')->first()->value;
    }
    if ($profile->hasField('field_email') && !$profile->get('field_email')->isEmpty()) {
      $result['email'] = $profile->get('field_email')->first()->value;
    }

    return $result;
  }

  /**
   * Get the basket array for the commerce order.
   *
   * @param OrderInterface $order
   *   The commerce order.
   * @return array
   *   Returns a full basket array.
   */
  private function getBasket(OrderInterface $order) {
    $basket = [];
    foreach ($order->getItems() as $key => $order_item) {
      $basket[$key] = $this->getLineItem($order_item);
    }
    return $basket;
  }

  /**
   * Get a line item array for the commerce order.
   *
   * @param OrderItemInterface $order_item
   *   The commerce order.
   * @return array
   *   Returns a full line item array.
   */
  private function getLineItem($order_item) {
    /** @var ProductVariationInterface $product_variation */
    $product_variation = $order_item->getPurchasedEntity();

    $vat_rate = 0;
    /** @var \Drupal\commerce_order\Adjustment $adjustment */
    foreach ($order_item->getAdjustments() as $adjustment) {
      if ($adjustment->getSourceId() == 'moms|dk|standard') {
        $vat_rate = $adjustment->getPercentage();
        break;
      }
    }

    $result = [
      'qty' => (integer) $order_item->getQuantity(),
      'item_no' => $product_variation->getSku(),
      'item_name' => $product_variation->getTitle(),
      'item_price' => (integer) str_replace('.', '', $this->rounder->round($order_item->getUnitPrice())->getNumber()),
      'vat_rate' => (float) $vat_rate,
    ];

    return $result;

  }

  /**
   * Generate a QuickPay payment link.
   *
   * @param PaymentInterface $payment
   *   The commerce payment.
   * @param array $additional_parameters
   *   An array of parameters to set on the payment link.
   *   This allows for overriding some of the automatically set parameters.
   * @return \Drupal\quickpay\Response\QuickPayPaymentLinkResponse
   *   Returns a reponse object for the payment link creation.
   */
  private function generateLink(PaymentInterface $payment, array $additional_parameters) {
    // Check for existing link
    $existing_link = $this->service->getPaymentLink($payment->getRemoteId());
    if ($existing_link->success) {
      // Payment already has a link, delete it, we want a fresh one.
      $this->service->deletePaymentLink($payment->getRemoteId());
    }

    // Create a new payment link.
    $parameters = [
      'id' => $payment->getRemoteId(),
      'amount' => (integer) str_replace('.', '', $this->rounder->round($payment->getAmount())->getNumber()),  // QuickPay only accepts 2 decimals
      'language' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
      'payment_methods' => $this->configuration['payment_methods'],
      'auto_capture' => $this->configuration['autocapture'],
      'auto_fee' => $this->configuration['autofee'],
      'customer_email' => $payment->getOrder()->getEmail(),
      'callback_url' => $this->getNotifyUrl()->toString(),
    ];
    $parameters = array_merge($parameters, $additional_parameters);

    // Append payment ID to cancel and return URLS
    // This is only to make loading the payment on the continue and cancel pages, much simpler.
    if (isset($parameters['continue_url'])) {
      $parameters['continue_url'] .= '?payment='.$payment->id();
    }
    if (isset($parameters['cancel_url'])) {
      $parameters['cancel_url'] .= '?payment='.$payment->id();
    }

    $payment_link = $this->service->createPaymentLink($payment->getRemoteId(), $parameters);

    return $payment_link;
  }

  /**
   * Signs the base content with the private key and returns a checksum.
   *
   * @param $base
   *   The base data being signed.
   * @param $private_key
   *   The private key to use for signing.
   * @return string
   *   Returns the signed checksum generated.
   */
  private function sign($base, $private_key) {
    return hash_hmac("sha256", $base, $private_key);
  }

  /**
   * @param integer $order_id
   *   The order id.
   * @param integer $remote_id
   *   The remote id.
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   The newest payment entity for the order.
   */
  private function getNewestPayment($order_id, $remote_id = NULL) {
    $payments_query = \Drupal::database()->select('commerce_payment', 'cp');
    $payments_query->fields('cp', ['payment_id']);
    $payments_query->condition('cp.order_id', $order_id);
    $payments_query->condition('cp.state', 'new');
    if (!is_null($remote_id)) {
      $payments_query->condition('cp.remote_id', $remote_id);
    }
    $payments_query->orderBy('cp.payment_id', 'DESC');

    $payments = $payments_query->execute()->fetchAssoc();
    $payments = reset($payments);

    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    try {
      $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load($payments);
    } catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('Commerce QuickPay')->critical('<strong>'.$e->getMessage().'</strong> : <br /><pre>'.$e->getTraceAsString().'</pre>');
    }

    return $payment;
  }
}