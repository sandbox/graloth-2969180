<?php

namespace Drupal\commerce_quickpay\PluginForm\OffsiteRedirect;


use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class QuickpayOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_quickpay\Plugin\Commerce\PaymentGateway\Quickpay $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $parameters = [
      'continue_url' => $form['#return_url'],
      'cancel_url' => $form['#cancel_url']
    ];

    $quickpay_url = $payment_gateway_plugin->createRemotePayment($payment, $parameters);

    return $this->buildRedirectForm($form, $form_state, $quickpay_url, [], BasePaymentOffsiteForm::REDIRECT_GET);
  }
}