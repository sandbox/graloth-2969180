<?php

namespace Drupal\quickpay\Response;


class QuickPayPaymentsResponse extends QuickPayResponse {

  /** @var array */
  public $payments;
}