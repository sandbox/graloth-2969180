<?php

namespace Drupal\quickpay\Response;


class QuickPayPaymentResponse extends QuickPayResponse {

  /** @var \stdClass */
  public $payment;
}