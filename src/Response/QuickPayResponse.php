<?php

namespace Drupal\quickpay\Response;


class QuickPayResponse {

  /**
   * Whether the action that triggered the response was a success.
   *
   * @var bool
   */
  public $success;

  /**
   * The status code of the response.
   *
   * @var string|integer
   */
  public $code;

  /**
   * The message of the reponse, is a translatable string.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public $message;

  /**
   * The error message, is not translatable, as it contains the errors
   * from QuickPay, parsed to make them human readable.
   *
   * @var string
   */
  public $error;
}