<?php

namespace Drupal\quickpay\Response;


class QuickPayPaymentLinkResponse extends QuickPayResponse {

  public $url;
}