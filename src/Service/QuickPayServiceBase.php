<?php

namespace Drupal\quickpay\Service;


use Drupal\quickpay\Response\QuickPayResponse;

class QuickPayServiceBase {

  /**
   * The logger for the QuickPay service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new QuickPayHelpers object.
   */
  public function __construct() {
    $this->logger = \Drupal::logger('QuickPay Service');
  }

  /**
   * Formats the responses error message into a human readable format.
   *
   * @param \QuickPay\API\Response $response
   *   The response to read the error from.
   * @return string
   *   Returns the error message from QuickPay in a human readable format.
   */
  protected function formatErrorResponse($response) {
    if ($decoded = json_decode($response->asRaw()[2])) {
      $message = $decoded->message . '!';
      $fields = [];
      if (!empty($decoded->errors)) {
        foreach ($decoded->errors as $id => $errors) {
          $error_types = [];
          foreach ($errors as $error) {
            $error_types[] = $error;
          }
          $fields[] = '(' . $id . ' ' . implode(', ', $error_types). ')';
        }
      }
      return $message . ' ' . implode(', ', $fields);
    } else {
      return 'Unknown error. HTTP response status '. $response->httpStatus();
    }
  }

  /**
   * Flattens and concatenates an array according to what QuickPay expects as parameters.
   *
   * @param array $parameters
   *   The array to be flattened.
   * @param string $prefix
   *   The prefix to add in front of every key.
   * @return array
   *   Returns a flattened array with concatenated keys.
   */
  public function formatParameterArray(array $parameters, $prefix = '') {
    $result = [];

    foreach ($parameters as $key => $value) {
      if (empty($prefix)) {
        $new_key = $key;
      } else {
        $new_key = $prefix.'['.$key.']';
      }

      if (is_array($value)) {
        $result = array_merge($result, $this->formatParameterArray($value, $new_key));
      } else {
        $result[$new_key] = $value;
      }
    }

    return $result;
  }

  /**
   * Logs any errors in the response to the logger.
   *
   * @param QuickPayResponse $response
   *   The response to use for logging.
   */
  protected function logErrors(QuickPayResponse $response) {
    if (!$response->success) {
      $this->logger->warning($response->message . ': ' . $response->error);
    } else {
      $this->logger->info($response->message);
    }
  }
}