<?php

namespace Drupal\quickpay\Service;

use Drupal\quickpay\Response\QuickPayPaymentLinkResponse;
use Drupal\quickpay\Response\QuickPayPaymentResponse;
use Drupal\quickpay\Response\QuickPayPaymentsResponse;
use Drupal\quickpay\Response\QuickPayResponse;
use Drupal\quickpay\Response\QuickPayScopeResponse;
use Money\Currency;
use Money\Money;
use QuickPay\API\Response;
use QuickPay\QuickPay;

/**
 * Class QuickPayService.
 */
class QuickPayService extends QuickPayServiceBase implements QuickPayServiceInterface {

  /**
   * The API Key used to make calls to QuickPay's API.
   *
   * @var string
   */
  private $apiKey;

  /**
   * The Private Key used to authenticate callback responses from QuickPay.
   *
   * @var string
   */
  private $privateKey;

  /**
   * The QuickPay client.
   *
   * @var QuickPay
   */
  private $client;

  /**
   * @inheritdoc
   */
  public function setCredentials($apiKey, $privateKey) {
    $this->apiKey = $apiKey;
    $this->privateKey = $privateKey;

    $this->client = new QuickPay(':' . $this->apiKey);

    return $this;
  }

  /**
   * @inheritdoc
   */
  public function hasCredentials() {
    if (!empty($this->apiKey) && !empty($this->privateKey)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * @inheritdoc
   */
  public function generateChecksum($content) {
    return hash_hmac('sha256', $content, $this->privateKey);
  }

  /**
   * @inheritdoc
   */
  public function getApiKeyScope() {
    $response = $this->client->request->get('/ping');

    $result = new QuickPayScopeResponse();
    switch ($response->httpStatus()) {
      case 200:
        $result->success = TRUE;
        $result->code = $response->httpStatus();
        $result->message = t('Scope retrieved.', [], ['context' => 'QuickPay API Service']);
        $result->scope = $response->asObject()->scope;
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function getPayments($order_id, $wildcard = FALSE, $additional_parameters = []) {
    $wildcard_suffix = $wildcard? '*' : '';

    $payments = [];

    /** @var Response $response */
    $response =  $this->client->request->get('/payments', ['order_id' => (string) $order_id.$wildcard_suffix] + $additional_parameters);

    $result = new QuickPayPaymentsResponse();
    switch ($response->httpStatus()) {
      case 200:

        foreach ($response->asObject() as $key => $payment) {
          $payments[$key] = $payment;
        }

        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment request successful.', [], ['context' => 'QuickPay API Service']);
        $result->payments   = $payments;
        break;
      case 403:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function getPayment($payment_id) {
    $response = $this->client->request->get('/payments', ['id' => (string) $payment_id]);

    $result = new QuickPayPaymentResponse();
    switch ($response->httpStatus()) {
      case 200:
        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment found.', [], ['context' => 'QuickPay API Service']);
        $result->payment    = $response->asObject();
        break;
      case 403:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function createPayment(array $parameters) {
    $response = $this->client->request->post('/payments', $this->formatParameterArray($parameters));

    $result = new QuickPayPaymentResponse();
    switch ($response->httpStatus()) {
      case 201:
        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment created.', [], ['context' => 'QuickPay API Service']);
        $result->payment    = $response->asObject();
        break;
      case 400:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Invalid parameters.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      case 403:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function updatePayment($payment_id, $parameters) {
    $response = $this->client->request->patch('/payments/' . $payment_id, $this->formatParameterArray($parameters));

    $result = new QuickPayPaymentResponse();
    switch ($response->httpStatus()) {
      case 200:
        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment updated.', [], ['context' => 'QuickPay API Service']);
        $result->payment    = $response->asObject();
        break;
      case 403:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      case 404:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment not found.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function getPaymentLink($payment_id) {
    $response = $this->getPayment($payment_id);

    $result = new QuickPayPaymentLinkResponse();

    if ($response->success) {
      $payment = $response->payment;

      if (!empty($payment->link->url)) {
        $result->success    = TRUE;
        $result->code       = $response->code;
        $result->message    = t('Payment link found.', [], ['context' => 'QuickPay API Service']);
        $result->url        = $payment->link->url;
      } else {
        $result->success    = FALSE;
        $result->code       = $response->code;
        $result->message    = t('No payment link.', [], ['context' => 'QuickPay API Service']);
        $result->error      = 'Payment found, but has no link.';
      }
    } else {
      $result->success    = $response->success;
      $result->code       = $response->code;
      $result->message    = $response->message;
      $result->error      = $response->error;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function createPaymentLink($payment_id, array $parameters) {
    $response = $this->client->request->put('/payments/' . $payment_id . '/link', $this->formatParameterArray($parameters));

    $result = new QuickPayPaymentLinkResponse();
    switch ($response->httpStatus()) {
      case 200:
        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment link created.', [], ['context' => 'QuickPay API Service']);
        $result->url        = $response->asObject()->url;
        break;
      case 400:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Invalid parameters.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function deletePaymentLink($payment_id) {
    $response = $this->client->request->delete('/payments/' . $payment_id . '/link');

    $result = new QuickPayResponse();
    switch ($response->httpStatus()) {
      case 204:
        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment link deleted.', [], ['context' => 'QuickPay API Service']);
        break;
      case 400:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Invalid parameters.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function authorizePayment($payment_id, Money $amount, array $additional_parameters = []) {
    $result = new QuickPayPaymentResponse();

    $parameters = [
      'id' => $payment_id,
      'amount' => $amount->getAmount()
    ] + $additional_parameters;

    $response = $this->client->request->post('/payments/'.$payment_id.'/authorize', $parameters);

    switch ($response->httpStatus()) {
      case 202:
        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment authorization successful.', [], ['context' => 'QuickPay API Service']);
        // Get the update payment data, as payment object in response data does not contain the
        // authorization data, and we want that to be included, to allow further validation.
        $result->payment = $this->getPayment($payment_id);
        break;
      case 403:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function capturePayment($payment_id, Money $amount = NULL) {
    $payment = $this->getPayment($payment_id);
    $balance = new Money($payment->payment->balance , new Currency($payment->payment->currency));

    $result = new QuickPayPaymentResponse();

    if ($payment->success) {
      // Check if payment is authorized.
      $authorized = FALSE;
      $authorized_amount = new Money(0, new Currency($payment->payment->currency));
      $cancelled = FALSE;

      if (!empty($payment->payment->operations)) {

        foreach ($payment->payment->operations as $operation) {

          if ($operation->type === 'cancel') {
            $cancelled = TRUE;
            $authorized = FALSE;
            break;
          }

          if ($operation->type === 'authorize' && $operation->pending === FALSE) {
            $authorized = TRUE;
            $authorized_amount = new Money($operation->amount, new Currency($payment->payment->currency));

            // If amount is empty, get it from the payment operation.
            if (empty($amount)) {
              // Set amount to the full authorized amount.
              $amount = new Money($operation->amount, new Currency($payment->payment->currency));
            }
          }
        }
      }

      if ($authorized) {

        if ($amount->lessThanOrEqual($authorized_amount->subtract($balance))) {
          $parameters = [
            'id' => $payment_id,
            'amount' => $amount->getAmount()
          ];
          $response = $this->client->request->post('/payments/'.$payment_id.'/capture', $parameters);

          switch ($response->httpStatus()) {
            case 202:
              $result->success    = TRUE;
              $result->code       = $response->httpStatus();
              // Get the update payment data, as payment object in response data does not contain the
              // capture data, and we want that to be included, to allow further validation.
              $payment = $this->getPayment($payment_id);
              // Check balance to give better reponse
              $balance = new Money($payment->payment->balance , new Currency($payment->payment->currency));

              if ($balance->greaterThanOrEqual($authorized_amount)) {
                $result->message    = t('Payment fully captured, no further payment capture possible.', [], ['context' => 'QuickPay API Service']);
              } else {
                $result->message    = t('Partial payment capture successful.', [], ['context' => 'QuickPay API Service']);
              }

              $result->payment = $payment;

              break;
            case 400:
              $result->success    = FALSE;
              $result->code       = $response->httpStatus();
              $result->message    = t('Invalid parameters.', [], ['context' => 'QuickPay API Service']);
              $result->error      = $this->formatErrorResponse($response);
              break;
            case 403:
              $result->success    = FALSE;
              $result->code       = $response->httpStatus();
              $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
              $result->error      = $this->formatErrorResponse($response);
              break;
            default:
              $result->success    = FALSE;
              $result->code       = $response->httpStatus();
              $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
              $result->error      = $this->formatErrorResponse($response);
              break;
          }
        } else {
          $result->success    = FALSE;
          $result->code       = $payment->code;
          $result->message    = t('Capture amount too high.', [], ['context' => 'QuickPay API Service']);
          $result->error      = 'Unable to capture payment, capture amount higher than remaining authorized amount.';
        }
      } else {

        if ($cancelled) {
          $result->success    = FALSE;
          $result->code       = $payment->code;
          $result->message    = t('Unable to capture.', [], ['context' => 'QuickPay API Service']);
          $result->error      = 'Unable to capture payment on cancelled payment.';
        } else {
          $result->success    = FALSE;
          $result->code       = $payment->code;
          $result->message    = t('Unable to capture.', [], ['context' => 'QuickPay API Service']);
          $result->error      = 'Unable to capture payment when payment is not authorized.';
        }
      }

    } else {
      $result->success    = $payment->success;
      $result->code       = $payment->code;
      $result->message    = $payment->message;
      $result->error      = $payment->error;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function refundPayment($payment_id, Money $amount = NULL) {
    $payment = $this->getPayment($payment_id);
    $balance = new Money($payment->payment->balance , new Currency($payment->payment->currency));

    $result = new QuickPayPaymentResponse();

    if ($payment->success) {
      $authorized = FALSE;
      $cancelled = FALSE;

      if (!empty($payment->payment->operations)) {

        foreach ($payment->payment->operations as $operation) {

          if ($operation->type === 'cancel') {
            $cancelled = TRUE;
            $authorized = FALSE;
            break;
          }

          if ($operation->type === 'authorize' && $operation->pending === FALSE) {
            $authorized = TRUE;

            // If amount is empty, get it from the payment operation.
            if (empty($amount)) {
              // Set amount to the full authorized amount.
              $amount = $balance;
            }
          }
        }
      }

      if ($authorized) {

        if ($amount->lessThanOrEqual($balance)) {
          $parameters = [
            'id' => $payment_id,
            'amount' => $amount->getAmount()
          ];
          $response = $this->client->request->post('/payments/'.$payment_id.'/refund', $parameters);

          switch ($response->httpStatus()) {
            case 202:
              $result->success    = TRUE;
              $result->code       = $response->httpStatus();

              // Get the update payment data, as payment object in response data does not contain the
              // refund data, and we want that to be included, to allow further validation.
              $payment = $this->getPayment($payment_id);

              // Check balance to give better reponse
              $balance = new Money($payment->payment->balance , new Currency($payment->payment->currency));

              if ($balance->isZero()) {
                $result->message    = t('Payment fully refunded, no further payment refunds possible.', [], ['context' => 'QuickPay API Service']);
              } else {
                $result->message    = t('Partial payment refund successful.', [], ['context' => 'QuickPay API Service']);
              }

              $result->payment = $payment;

              break;
            case 400:
              $result->success    = FALSE;
              $result->code       = $response->httpStatus();
              $result->message    = t('Invalid parameters.', [], ['context' => 'QuickPay API Service']);
              $result->error      = $this->formatErrorResponse($response);
              break;
            case 403:
              $result->success    = FALSE;
              $result->code       = $response->httpStatus();
              $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
              $result->error      = $this->formatErrorResponse($response);
              break;
            default:
              $result->success    = FALSE;
              $result->code       = $response->httpStatus();
              $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
              $result->error      = $this->formatErrorResponse($response);
              break;
          }
        } else {
          $result->success    = FALSE;
          $result->code       = $payment->code;
          $result->message    = t('Refund amount amount too high.', [], ['context' => 'QuickPay API Service']);
          $result->error      = 'Unable to refund payment, refund amount higher than remaining balance.';
        }
      } else {

        if ($cancelled) {
          $result->success    = FALSE;
          $result->code       = $payment->code;
          $result->message    = t('Unable to refund.', [], ['context' => 'QuickPay API Service']);
          $result->error      = 'Unable to refund payment on cancelled payment.';
        } else {
          $result->success    = FALSE;
          $result->code       = $payment->code;
          $result->message    = t('Unable to refund.', [], ['context' => 'QuickPay API Service']);
          $result->error      = 'Unable to refund payment when payment is not authorized.';
        }
      }

    } else {
      $result->success    = $payment->success;
      $result->code       = $payment->code;
      $result->message    = $payment->message;
      $result->error      = $payment->error;
    }

    $this->logErrors($result);

    return $result;
  }

  /**
   * @inheritdoc
   */
  public function cancelPayment($payment_id) {
    $response = $this->client->request->post('/payments/'.$payment_id.'/cancel');

    $result = new QuickPayPaymentResponse();

    switch ($response->httpStatus()) {
      case 202:
        $result->success    = TRUE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Payment cancellation successful.', [], ['context' => 'QuickPay API Service']);
        // Get the update payment data, as payment object in response data does not contain the
        // cancellation data, and we want that to be included, to allow further validation.
        $result->payment    = $this->getPayment($payment_id);
        break;
      case 400:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Invalid parameters.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      case 403:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Not authorized.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
      default:
        $result->success    = FALSE;
        $result->code       = $response->httpStatus();
        $result->message    = t('Unknown error.', [], ['context' => 'QuickPay API Service']);
        $result->error      = $this->formatErrorResponse($response);
        break;
    }

    return $result;
  }
}