<?php

namespace Drupal\quickpay\Service;

use Drupal\quickpay\Response\QuickPayPaymentLinkResponse;
use Drupal\quickpay\Response\QuickPayPaymentResponse;
use Drupal\quickpay\Response\QuickPayResponse;
use Drupal\quickpay\Response\QuickPayScopeResponse;
use Money\Money;

/**
 * Interface QuickPayServiceInterface.
 */
interface QuickPayServiceInterface {

  /**
   * Sets the credentials used to communicate with QuickPay's API.
   *
   * @param string $apiKey
   *   The API Key used to make calls to QuickPay's API.
   * @param string $privateKey
   *   The Private Key used to authenticate callback responses from QuickPay.
   * @return QuickPayServiceInterface
   *   Returns the QuickPay service.
   */
  function setCredentials($apiKey, $privateKey);

  /**
   * Check if the service has the credentials set.
   *
   * @return bool
   *   Returns TRUE if both the API Key and Private Key are set.
   */
  function hasCredentials();

  /**
   * Generates a hashed checksum based on $content and the Private Key.
   *
   * @param $content
   *   The data to be hashed.
   * @return string
   *   The checksum string.
   */
  function generateChecksum($content);

  /**
   * Check the scope of the QuickPay API key.
   *
   * @return QuickPayScopeResponse
   *   If successful returns an object with the scope,
   *   otherwise returns and object with error information.
   */
  function getApiKeyScope();

  /**
   * Gets the payments associated with the order id.
   *
   * @param string $order_id
   *   The order id to check for payments on.
   * @param bool $wildcard
   *   Whether or not to suffix the order id with a wildcard to find all payments
   *   associated with an order that starts with $order_id.
   * @param array $additional_parameters
   *   An array with any additional parameters to search with.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payments found,
   *   otherwise it returns an object with error information.
   */
  function getPayments($order_id, $wildcard = FALSE, $additional_parameters = []);

  /**
   * Get a QuickPay payment.
   *
   * @param string $payment_id
   *   The QuickPay payment id to get payment for.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payment,
   *   otherwise it returns an object with error information.
   */
  public function getPayment($payment_id);

  /**
   * Create a QuickPay payment.
   *
   * @param array $parameters
   *   The parameter array to be used in creating the payment.
   *   Will be formatted according to QuickPay's requirements.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payment,
   *   otherwise it returns an object with error information.
   */
  public function createPayment(array $parameters);

  /**
   * Update a QuickPay payment.
   *
   * @param $payment_id
   *   The payment id to be updated.
   * @param array $parameters
   *   The parameter array to be used in creating the payment.
   *   Will be formatted according to QuickPay's requirements.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payment,
   *   otherwise it returns an object with error information.
   */
  public function updatePayment($payment_id, $parameters);

  /**
   * Gets a payment link for the payment id.
   *
   * @param string $payment_id
   *   The payment id for which to get the payment link.
   * @return QuickPayPaymentLinkResponse
   *   If successful returns an object with the payment url,
   *   otherwise it returns an object with error information.
   */
  public function getPaymentLink($payment_id);

  /**
   * Create a payment link for the payment id.
   *
   * @param string $payment_id
   *   The payment id for which to create the payment link.
   * @param array $parameters
   *   The parameters to use for creating the payment link.
   *   Will be formatted according to QuickPay's requirements.
   * @return QuickPayPaymentLinkResponse
   *   If successful returns an object with the payment url,
   *   otherwise it returns an object with error information.
   */
  public function createPaymentLink($payment_id, array $parameters);

  /**
   * Delete a payment link for the payment id.
   *
   * @param string $payment_id
   *   The payment id for which to delete the payment link.
   * @return QuickPayResponse
   *   If successful returns an object with success information,
   *   otherwise it returns an object with error information.
   */
  public function deletePaymentLink($payment_id);

  /**
   * Authorizes a payment.
   * Only required if handling card input and validation yourself.
   *
   * @param string $payment_id
   *   The payment id for which to authorize payment.
   * @param Money $amount
   *   The amount to authorize.
   * @param array $additional_parameters
   *   The additional parameters to use for authorizing the payment.
   *   Will be formatted according to QuickPay's requirements.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payment,
   *   otherwise it returns an object with error information.
   */
  public function authorizePayment($payment_id, Money $amount, array $additional_parameters = []);

  /**
   * Captures the given payment.
   *
   * Only payments with a succeeded authorization operation can be captured.
   *
   * @param string $payment_id
   *   The payment id for which to capture payment.
   * @param Money|null $amount
   *   The amount to capture. If null, defaults to the entire payment amount.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payment,
   *   otherwise it returns an object with error information.
   */
  public function capturePayment($payment_id, Money $amount = NULL);

  /**
   * Refunds the given payment.
   *
   * Only payments with a succeeded authorization operation can be refunded.
   *
   * @param string $payment_id
   *   The payment id for which to refund payment.
   * @param Money|null $amount
   *   The amount to refund. If null, defaults to the entire payment available balance.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payment,
   *   otherwise it returns an object with error information.
   */
  public function refundPayment($payment_id, Money $amount = NULL);

  /**
   * Cancels the given payment.
   *
   * @param string $payment_id
   *   The payment id for which to cancel payment.
   * @return QuickPayPaymentResponse
   *   If successful, returns an object which contains the payment,
   *   otherwise it returns an object with error information.
   */
  public function cancelPayment($payment_id);
}